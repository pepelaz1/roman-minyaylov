// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved

#ifndef _DUPLICATIONMANAGER_H_
#define _DUPLICATIONMANAGER_H_

#include "CommonTypes.h"
#include <atlbase.h>
//
// Handles the task of duplicating an output.
//
class DUPLICATIONMANAGER
{
    public:
        DUPLICATIONMANAGER();
        ~DUPLICATIONMANAGER();
		_Success_(*Timeout == false && return == DUPL_RETURN_SUCCESS) DUPL_RETURN GetFrame(_Out_ FRAME_DATA* Data, _Out_ bool* Timeout);
        DUPL_RETURN DoneWithFrame();
		DUPL_RETURN InitDupl(_In_ DX_RESOURCES* pDxRes, UINT Output);
		void Destroy();
        DUPL_RETURN GetMouse(_Inout_ PTR_INFO* PtrInfo, _In_ DXGI_OUTDUPL_FRAME_INFO* FrameInfo, INT OffsetX, INT OffsetY);
		DUPL_RETURN DrawMouse(_In_ PTR_INFO* PtrInfo, ID3D11Texture2D* Frame);
        void GetOutputDesc(_Out_ DXGI_OUTPUT_DESC* DescPtr);
		DUPL_RETURN ProcessMonoMask(ID3D11Texture2D* Frame, bool IsMono, _Inout_ PTR_INFO* PtrInfo, _Out_ INT* PtrWidth, _Out_ INT* PtrHeight, _Out_ INT* PtrLeft, _Out_ INT* PtrTop, _Outptr_result_bytebuffer_(*PtrHeight * *PtrWidth * BPP) BYTE** InitBuffer, _Out_ D3D11_BOX* Box);

		DUPL_RETURN Capture(unsigned char **p, int *width, int *height);

    private:
		PTR_INFO m_PtrInfo;
    // vars
        IDXGIOutputDuplication* m_DeskDupl;
        ID3D11Texture2D* m_AcquiredDesktopImage;
        _Field_size_bytes_(m_MetaDataSize) BYTE* m_MetaDataBuffer;
        UINT m_MetaDataSize;
        UINT m_OutputNumber;
        DXGI_OUTPUT_DESC m_OutputDesc;
        ID3D11Device* m_Device;
		CComPtr<ID3D11DeviceContext> m_DeviceContext;
		CComPtr<ID3D11VertexShader> m_VertexShader;
		CComPtr<ID3D11PixelShader> m_PixelShader;
		CComPtr<ID3D11InputLayout> m_InputLayout;
		CComPtr<ID3D11RenderTargetView> m_RTV;
		ID3D11SamplerState * m_SamplerLinear;
		CComPtr<ID3D11BlendState> m_BlendState;
		CComPtr<IDXGISwapChain1> m_SwapChain;

		void ExtractBitmap(void* texture, unsigned char **p, int *width, int *height);
		DUPL_RETURN MakeRTV();
};

#endif
