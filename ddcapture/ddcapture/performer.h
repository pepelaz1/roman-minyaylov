#pragma once
#include "DuplicationManager.h"
#include <atlbase.h>

class Performer
{
private:
	DUPLICATIONMANAGER DuplMgr;
	DX_RESOURCES DxRes;
	
	DUPL_RETURN InitializeDx(_Out_ DX_RESOURCES* Data);
	void CleanDx(_Inout_ DX_RESOURCES* Data);
	void Clean();
public:
	Performer();
	~Performer();

	int Init(int outnum);
	void Destroy();
	int Capture(unsigned char **p, int *width, int *height);
};

