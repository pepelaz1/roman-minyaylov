extern "C"
{
	__declspec(dllexport) int __cdecl Init(int outnum = 1);
	__declspec(dllexport) int __cdecl Capture(unsigned char **p, int *width, int *height);
	__declspec(dllexport) void __cdecl Destroy();
}