// testapp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include "conio.h"
#include <iostream>
using namespace std;
#include "../ddcapture/ddcapture.h"

double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double(li.QuadPart) / 1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}
double GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return double(li.QuadPart - CounterStart) / PCFreq;
}

int nn = 0;

int _tmain(int argc, _TCHAR* argv[])
{
	int times = 1000;
	cout << "Start processing " << times << " times... \n";
	StartCounter();
	//for (int i = 0; i < times; i++)
	//{

	unsigned char *p;
	int width, height;


	if (Init(1) == 0)
	{

		for (int i = 0; i < times; i++)
		{			
			cout << "Attempt " << i << "\r";
			Capture(&p, &width, &height);

			/*	char filename[256];
				sprintf(filename, "d:\\test\\%d.raw", nn++);

				size_t msize = width* 4 * height;
				FILE* pFile;
				pFile = fopen(filename, "wb");
				fwrite(p, msize, 1, pFile);
				fclose(pFile);*/

			delete p;

		}
		Destroy();
	}





	/*for (int i = 0; i < times; i++)
	{
	cout << "Attempt " << i << "\r";
	Init(1);
	unsigned char *p1;
	int width1, height1;
	Capture(&p1, &width1, &height1);
	delete p1;
	Destroy();
	}*/
	cout << "Total time: " << GetCounter() << " ms \n";
	_getch();

	return 0;
}

