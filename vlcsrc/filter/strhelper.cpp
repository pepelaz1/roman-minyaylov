#include "strhelper.h"



string char2hex( char dec )
{
    char dig1 = (dec&0xF0)>>4;
    char dig2 = (dec&0x0F);
    if ( 0<= dig1 && dig1<= 9) dig1+=48;    //0,48inascii
    if (10<= dig1 && dig1<=15) dig1+=97-10; //a,97inascii
    if ( 0<= dig2 && dig2<= 9) dig2+=48;
    if (10<= dig2 && dig2<=15) dig2+=97-10;

    string r;
    r.append( &dig1, 1);
    r.append( &dig2, 1);
    return r;
}



void cp1251_to_utf8(char *out, const char *in) 
{
    const char table[128][5] = {
        "\2\xD0\x82","\2\xD0\x83","\3\xE2\x80\x9A","\2\xD1\x93",
        "\3\xE2\x80\x9E","\3\xE2\x80\xA6","\3\xE2\x80\xA0","\3\xE2\x80\xA1",
        "\3\xE2\x82\xAC","\3\xE2\x80\xB0","\2\xD0\x89","\3\xE2\x80\xB9",
        "\2\xD0\x8A","\2\xD0\x8C","\2\xD0\x8B","\2\xD0\x8F",
        "\2\xD1\x92","\3\xE2\x80\x98","\3\xE2\x80\x99","\3\xE2\x80\x9C",
        "\3\xE2\x80\x9D","\3\xE2\x80\xA2","\3\xE2\x80\x93","\3\xE2\x80\x94",
        "\0","\3\xE2\x84\xA2","\2\xD1\x99","\3\xE2\x80\xBA",
        "\2\xD1\x9A","\2\xD1\x9C","\2\xD1\x9B","\2\xD1\x9F",
        "\2\xC2\xA0","\2\xD0\x8E","\2\xD1\x9E","\2\xD0\x88",
        "\2\xC2\xA4","\2\xD2\x90","\2\xC2\xA6","\2\xC2\xA7",
        "\2\xD0\x81","\2\xC2\xA9","\2\xD0\x84","\2\xC2\xAB",
        "\2\xC2\xAC","\2\xC2\xAD","\2\xC2\xAE","\2\xD0\x87",
        "\2\xC2\xB0","\2\xC2\xB1","\2\xD0\x86","\2\xD1\x96",
        "\2\xD2\x91","\2\xC2\xB5","\2\xC2\xB6","\2\xC2\xB7",
        "\2\xD1\x91","\3\xE2\x84\x96","\2\xD1\x94","\2\xC2\xBB",
        "\2\xD1\x98","\2\xD0\x85","\2\xD1\x95","\2\xD1\x97",
        "\2\xD0\x90","\2\xD0\x91","\2\xD0\x92","\2\xD0\x93",
        "\2\xD0\x94","\2\xD0\x95","\2\xD0\x96","\2\xD0\x97",
        "\2\xD0\x98","\2\xD0\x99","\2\xD0\x9A","\2\xD0\x9B",
        "\2\xD0\x9C","\2\xD0\x9D","\2\xD0\x9E","\2\xD0\x9F",
        "\2\xD0\xA0","\2\xD0\xA1","\2\xD0\xA2","\2\xD0\xA3",
        "\2\xD0\xA4","\2\xD0\xA5","\2\xD0\xA6","\2\xD0\xA7",
        "\2\xD0\xA8","\2\xD0\xA9","\2\xD0\xAA","\2\xD0\xAB",
        "\2\xD0\xAC","\2\xD0\xAD","\2\xD0\xAE","\2\xD0\xAF",
        "\2\xD0\xB0","\2\xD0\xB1","\2\xD0\xB2","\2\xD0\xB3",
        "\2\xD0\xB4","\2\xD0\xB5","\2\xD0\xB6","\2\xD0\xB7",
        "\2\xD0\xB8","\2\xD0\xB9","\2\xD0\xBA","\2\xD0\xBB",
        "\2\xD0\xBC","\2\xD0\xBD","\2\xD0\xBE","\2\xD0\xBF",
        "\2\xD1\x80","\2\xD1\x81","\2\xD1\x82","\2\xD1\x83",
        "\2\xD1\x84","\2\xD1\x85","\2\xD1\x86","\2\xD1\x87",
        "\2\xD1\x88","\2\xD1\x89","\2\xD1\x8A","\2\xD1\x8B",
        "\2\xD1\x8C","\2\xD1\x8D","\2\xD1\x8E","\2\xD1\x8F"
    };
    while (*in) {
        if (*in & 0x80) {
            const char *p = table[(int)(0x7f & *in++)];
            if (!*p)
                continue;
            *out++ = p[1];
            if (*p == 1)
                continue;
            *out++ = p[2];
            if (*p == 2)
                continue;
            *out++ = p[3];
        }
        else
            *out++ = *in++;
    }
    *out = 0;
}


//based on javascript encodeURIComponent()
string urlencode(const string &c)
{
    
    string escaped="";
    int max = c.length();
    for(int i=0; i<max; i++)
    {
        if ( (48 <= c[i] && c[i] <= 57) ||//0-9
             (65 <= c[i] && c[i] <= 90) ||//abc...xyz
             (97 <= c[i] && c[i] <= 122) || //ABC...XYZ
             (c[i]=='~' || c[i]=='!' || c[i]=='*' || c[i]=='(' || c[i]==')' || c[i]=='\'' || c[i]=='/' || c[i]==':')
        )
        {
            escaped.append( &c[i], 1);
        }
        else
        {
            escaped.append("%");
            escaped.append( char2hex(c[i]) );//converts char 255 to string "ff"
        }
    }
    return escaped;
}


void trim(wstring& str)
{
  string::size_type pos = str.find_last_not_of(' ');
  if(pos != string::npos) {
    str.erase(pos + 1);
    pos = str.find_first_not_of(' ');
    if(pos != string::npos) str.erase(0, pos);
  }
  else str.erase(str.begin(), str.end());
}

void replace_with(wstring & src, const wstring & what, const wstring & with)
{    
    if (what != with) {
        wstring temp;
        wstring::size_type prev_pos = 0, pos = src.find(what, 0);
        while ( wstring::npos != pos ) {
            temp += wstring(src.begin() + prev_pos, src.begin() + pos) + with;
            prev_pos = pos + what.size();
            pos = src.find(what, prev_pos);
        }
        if ( !temp.empty() ) {
            src = temp + wstring(src.begin() + prev_pos, src.end());
            if (wstring::npos == with.find(what)) {
                replace_with(src, what, with);
            }
        }
    }
}

vector<wstring> split(wstring &str, wchar_t sep)
{
	vector<wstring> result;
	wstring s;

	int b = 0;
	int n = str.find(sep);
	for(;;)
	{
		if (n==-1)
		{
			s = str.substr(b,str.length()-b);
			if (s != L"")
				result.push_back(s);
			break;
		}
		s = str.substr(b,n-b);
		if (s != L"")
			result.push_back(s);
		b = n+1;
		n = str.find(sep,b);
	}
	return result;


}

wstring lower(wstring s)
{
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}
	
wstring make_str1(wstring ws1, int i1, wstring ws2)
{
	wstringstream wss;
	wss << ws1;
	wss << i1;
	wss << ws2;
	return wss.str();
}


