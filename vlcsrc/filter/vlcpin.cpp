#include <streams.h>
#include "vlcwrapper.h"
#include "vlcpin.h"
#include "filter.h"


CVlcPin::CVlcPin(HRESULT *phr, CSource *pFilter, WCHAR *pName, CVlcWrapper *pVlc)
        : CSourceStream(NAME("Vlc source filter"), phr, pFilter,pName),
		CSourceSeeking(NAME("SeekVlc"), (IPin*)this, phr, &m_cSharedState),
		m_pVlc(pVlc),
		m_bDiscont(TRUE)
{
}

CVlcPin::~CVlcPin()
{   
}

void CVlcPin::UpdateFromSeek()
{
	CFilter *flt = (CFilter *)m_pFilter;
	flt->UpdateFromSeek();

  /*  if (ThreadExists()) 
    {
        DeliverBeginFlush();
		Stop();
		m_pVlc->ClearVideoQueue();
		m_pVlc->ClearAudioQueue();
        // Shut down the thread and stop pushing data.

	
		
        DeliverEndFlush();
        // Restart the thread and start pushing data again.
        Pause();

		char str[MAX_PATH];
		sprintf(str,"CVlcPin::UpdateFromSeek\n");
		OutputDebugStringA(str);
    }*/
}

void CVlcPin::SeekStop()
{
	/*if (ThreadExists()) 
    {
        DeliverBeginFlush();
		Stop();
	//	m_pVlc->ClearVideoQueue();
	//	m_pVlc->ClearAudioQueue();
	 }*/
}

void CVlcPin::SeekStart()
{
	if (ThreadExists()) 
	{
		DeliverEndFlush();
		Pause();
	}
}


HRESULT CVlcPin::ChangeStart()
 {
	 UpdateFromSeek();
	 return S_OK;
 }

 HRESULT CVlcPin::ChangeStop()
 {
	 UpdateFromSeek();
	 return S_OK;
 }

 HRESULT CVlcPin::ChangeRate()
 {
	 return S_OK;
 }

 STDMETHODIMP CVlcPin::GetDuration(LONGLONG *pDuration)
 {
	 HRESULT hr = CSourceSeeking::GetDuration(pDuration);
	 if (FAILED(hr))
		 return hr;
	 *pDuration = m_pVlc->GetDuration();
	 return hr;
 }

STDMETHODIMP  CVlcPin::SetPositions(LONGLONG *pCurrent, DWORD CurrentFlags, LONGLONG *pStop,  DWORD StopFlags )
{
	m_pVlc->SetCurrentPosition(*pCurrent);

	HRESULT hr = CSourceSeeking::SetPositions(pCurrent, CurrentFlags, pStop, StopFlags);
	if (FAILED(hr))
		return hr;

	return hr;
}


//
// ThreadProc
//
// When this returns the thread exits
// Return codes > 0 indicate an error occured
DWORD CVlcPin::ThreadProc(void) {

    HRESULT hr;  // the return code from calls
    Command com;

    do {
	com = GetRequest();
	if (com != CMD_INIT) {
	    DbgLog((LOG_ERROR, 1, TEXT("Thread expected init command")));
	    Reply((DWORD) E_UNEXPECTED);
	}
    } while (com != CMD_INIT);

    DbgLog((LOG_TRACE, 1, TEXT("CSourceStream worker thread initializing")));

    hr = OnThreadCreate(); // perform set up tasks
    if (FAILED(hr)) {
        DbgLog((LOG_ERROR, 1, TEXT("CSourceStream::OnThreadCreate failed. Aborting thread.")));
        OnThreadDestroy();
	Reply(hr);	// send failed return code from OnThreadCreate
        return 1;
    }

    // Initialisation suceeded
    Reply(NOERROR);

    Command cmd;
    do {
	cmd = GetRequest();

	switch (cmd) {

	case CMD_EXIT:
	    Reply(NOERROR);
	    break;

	case CMD_RUN:
	    DbgLog((LOG_ERROR, 1, TEXT("CMD_RUN received before a CMD_PAUSE???")));
	    // !!! fall through???
	
	case CMD_PAUSE:
	    Reply(NOERROR);
	    DoBufferProcessingLoop();
	    break;

	case CMD_STOP:
		OnThreadStopPlay();
		Reply(NOERROR);
	    break;

	default:
	    DbgLog((LOG_ERROR, 1, TEXT("Unknown command %d received!"), cmd));
	    Reply((DWORD) E_NOTIMPL);
	    break;
	}
    } while (cmd != CMD_EXIT);

    hr = OnThreadDestroy();	// tidy up.
    if (FAILED(hr)) {
        DbgLog((LOG_ERROR, 1, TEXT("CSourceStream::OnThreadDestroy failed. Exiting thread.")));
        return 1;
    }

    DbgLog((LOG_TRACE, 1, TEXT("CSourceStream worker thread exiting")));
    return 0;
}