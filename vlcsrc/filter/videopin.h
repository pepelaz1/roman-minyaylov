#pragma once
#include "vlcpin.h"

class CVlcWrapper;

class CVideoPin : public CVlcPin
{
public:
	UINT m_frameno;
   // STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void **ppv);
protected:

   // CRefTime m_rtSampleTime;	        // The time stamp for each sample
    //REFERENCE_TIME m_rtFrameLength;
    CMediaType m_MediaType;
	int m_nRealWidth;
	int m_nRealHeight;
	int m_nStride;
   

	
	//unsigned char *m_pPrevFrame;

	HRESULT OnThreadStartPlay();
	HRESULT OnThreadStopPlay();
public:

    CVideoPin(HRESULT *phr, CSource *pFilter, CVlcWrapper *pVlc);
    ~CVideoPin();

    // Override the version that offers exactly one media type
    HRESULT DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *pRequest);
    HRESULT FillBuffer(IMediaSample *pSample);
    
    // Set the agreed media type and set up the necessary parameters
    HRESULT SetMediaType(const CMediaType *pMediaType);

    // Support multiple display formats
    HRESULT CheckMediaType(const CMediaType *pMediaType);
    HRESULT GetMediaType(int iPosition, CMediaType *pmt);

    // Quality control
	// Not implemented because we aren't going in real time.
	// If the file-writing filter slows the graph down, we just do nothing, which means
	// wait until we're unblocked. No frames are ever dropped.
    STDMETHODIMP Notify(IBaseFilter *pSelf, Quality q)
    {
        return S_OK;
    }
	void OnAudioStart();
	void SeekStop();
};
