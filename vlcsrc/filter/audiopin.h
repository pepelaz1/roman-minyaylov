#pragma once
#include "vlcpin.h"

class CVlcWrapper;

class CAudioPin : public CVlcPin
{
public:
    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void **ppv);
protected:
	UINT m_frameno;
    CRefTime m_rtSampleTime;	        // The time stamp for each sample
    REFERENCE_TIME m_rtFrameLength;
	REFERENCE_TIME m_rtStart;
	REFERENCE_TIME m_rtSync;

    CMediaType m_MediaType;
	HRESULT OnThreadStartPlay();
	HRESULT OnThreadStopPlay();
	HRESULT DoBufferProcessingLoop();
public:

    CAudioPin(HRESULT *phr, CSource *pFilter, CVlcWrapper *pVlc);
    ~CAudioPin();

    // Override the version that offers exactly one media type
    HRESULT DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *pRequest);
    HRESULT FillBuffer(IMediaSample *pSample);
    
    // Set the agreed media type and set up the necessary parameters
   // HRESULT SetMediaType(const CMediaType *pMediaType);

    // Support multiple display formats
    HRESULT CheckMediaType(const CMediaType *pMediaType);
    HRESULT GetMediaType(int iPosition, CMediaType *pmt);

	STDMETHODIMP Notify(IBaseFilter *pSelf, Quality q)
    {
        return S_OK;
    }
	void SeekStop();
};

