#include <streams.h>
#include <dvdmedia.h>
#include <InitGuid.h>
#include "videopin.h"
#include "audiopin.h"
#include "filter.h"
#include "vlcwrapper.h"
#include "logger.h"

CLogger g_log;


CFilter::CFilter(IUnknown *pUnk, HRESULT *phr)
           : CSource(NAME("VlcSource"), pUnk, CLSID_VlcSource) 
{
	m_pVlc = new CVlcWrapper();
	m_pVlc->Init();
	g_log.Reset();
}


CFilter::~CFilter()
{	
	DeletePins();
	delete m_pVlc;
}


CUnknown * WINAPI CFilter::CreateInstance(IUnknown *pUnk, HRESULT *phr)
{
    CFilter *pNewFilter = new CFilter(pUnk, phr);

	if (phr)
	{
		if (pNewFilter == NULL) 
			*phr = E_OUTOFMEMORY;
		else
			*phr = S_OK;
	}
    return pNewFilter;
}

STDMETHODIMP CFilter::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
	if (riid == IID_IVlcSrc) {
        return GetInterface((IVlcSrc *) this, ppv);
	}else if (riid == IID_IFileSourceFilter) {
        *ppv = static_cast<IFileSourceFilter *>(this);
		AddRef();
		return S_OK;

	} else {
		return CSource::NonDelegatingQueryInterface(riid, ppv);
	}
}


HRESULT CFilter::CreatePins()
{
	HRESULT hr = S_OK;
	DeletePins();
	
	if (m_pVlc->HasVideo())
	{
		CVideoPin *pin = new CVideoPin(&hr, this, m_pVlc);
		if (FAILED(hr))
			return hr;
		m_pins.push_back(pin);
	}

	if (m_pVlc->HasAudio())
	{
		CAudioPin *pin = new CAudioPin(&hr, this, m_pVlc);
		if (FAILED(hr))
			return hr;
		m_pins.push_back(pin);
	}
	return S_OK;
}

void CFilter::DeletePins()
{
	for(list<CVlcPin *>::iterator i = m_pins.begin(); 
	  i != m_pins.end(); i++)
	{
		CVlcPin *pin = *i;
		if (pin)
			delete pin;
	}
	m_pins.clear();
}

STDMETHODIMP CFilter::Run(REFERENCE_TIME tStart)
{
	m_pVlc->Run();

	HRESULT hr = CSource::Run(tStart);
	if (FAILED(hr))
		return hr;

	return hr;
}

STDMETHODIMP CFilter::Pause()
{
	FILTER_STATE fs;
	GetState(10, &fs);
	if (State_Stopped == fs)
	{
		//m_pVlc->Run();
	}
	else if (State_Running == fs)
	{
		m_pVlc->Pause();
	}

	HRESULT hr = CSource::Pause();
	if (FAILED(hr))
		return hr;

	//return hr;
	//return m_pVlc->Pause();
	return hr;
}

STDMETHODIMP CFilter::Stop()
{
	

	HRESULT hr = CSource::Stop();
	if (FAILED(hr))
		return hr;

	m_pVlc->Stop();

	return hr;
}


//
// IVlcSrc implementation
//
STDMETHODIMP CFilter::SetFile(WCHAR *file)
{
	HRESULT hr = m_pVlc->Load(file);
	if (FAILED(hr))
		return hr;

	hr = CreatePins();
	if (FAILED(hr))
		return hr;

	return hr;
}

STDMETHODIMP CFilter::GetAudioTracksCount(int *count)
{
	if (!m_pVlc)
	{
		*count = -1;
		return S_FALSE;
	}
	*count = m_pVlc->GetAudioTracksCount();
	return S_OK;
}

STDMETHODIMP CFilter::GetAudioTrackInfo(int number, int *id, WCHAR *text)
{
	if (!m_pVlc)
		return S_FALSE;

	m_pVlc->GetAudioTrackInfo(number,id, text);
	return S_OK;
}

STDMETHODIMP CFilter::GetAudioTrack(int *id)
{
	if (!m_pVlc)
	{
		*id = -1;
		return S_FALSE;
	}
	*id = m_pVlc->GetAudioTrack();
	return S_OK;
}

STDMETHODIMP CFilter::SetAudioTrack(int id)
{
	if (!m_pVlc)
		return S_FALSE;

	if (m_pVlc->SetAudioTrack(id) == -1)
		return E_FAIL;

	return S_OK;
}

STDMETHODIMP CFilter::GetSubtitlesCount (int *count)
{
	if (!m_pVlc)
	{
		*count = -1;
		return S_FALSE;
	}
	*count = m_pVlc->GetSubtitlesCount();
	return S_OK;
}

STDMETHODIMP CFilter::GetSubtitleInfo (int number, int *id, WCHAR *text)
{
	if (!m_pVlc)
		return S_FALSE;
	
	m_pVlc->GetSubtitleInfo(number,id, text);
	return S_OK;
}

STDMETHODIMP CFilter::GetSubtitle(int *id)
{
	if (!m_pVlc)
	{
		*id = -1;
		return S_FALSE;
	}
	*id = m_pVlc->GetSubtitle();
	return S_OK;
}

STDMETHODIMP CFilter::SetSubtitle(int id)
{
	if (!m_pVlc)
		return S_FALSE;

	if (m_pVlc->SetSubtitle(id) == -1)
		return E_FAIL;
	
	return S_OK;
}

STDMETHODIMP CFilter::GetCurFile(LPOLESTR *ppszFileName,AM_MEDIA_TYPE *pmt)
{
	if (!m_pVlc)
		return E_FAIL;
	
	WCHAR wfile[MAX_PATH];
	m_pVlc->GetFileName(wfile);
	
        DWORD n = sizeof(WCHAR)*(1+lstrlenW(wfile));

        *ppszFileName = (LPOLESTR) CoTaskMemAlloc( n );
        if (*ppszFileName!=NULL) {
                CopyMemory(*ppszFileName, wfile, n);
        }
   


	return S_OK;
}

STDMETHODIMP CFilter::Load(LPCOLESTR pszFileName, const AM_MEDIA_TYPE *pmt)
{	
	wstring file = pszFileName;
	return SetFile((WCHAR *)file.c_str());
}

void CFilter::UpdateFromSeek()
{
	for(list<CVlcPin *>::iterator i = m_pins.begin(); 
	  i != m_pins.end(); i++)
	{
		CVlcPin *pin = *i;
		pin->SeekStop();
	}

	for(list<CVlcPin *>::iterator i = m_pins.begin(); 
	  i != m_pins.end(); i++)
	{
		CVlcPin *pin = *i;
		pin->SeekStart();
	}
}