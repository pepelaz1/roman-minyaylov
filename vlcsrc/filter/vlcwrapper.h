#pragma once
#include <windows.h>
#include <wxdebug.h>
#include <wxutil.h>
#include <queue>
#include <list>

using namespace std;



struct libvlc_instance_t;
struct libvlc_media_t;
struct libvlc_media_player_t;
struct libvlc_media_track_info_t;
struct libvlc_track_description_t;
struct libvlc_event_manager_t;

struct AudioChunk
{
	unsigned char *buff;
	int size;
	LONGLONG pts;
};

class CVlcWrapper
{
private:
	libvlc_instance_t *m_pInstance;
	libvlc_media_t *m_pMedia;
	libvlc_media_player_t *m_pPlayer;
	libvlc_media_track_info_t *m_pTracksInfo;
	libvlc_track_description_t *m_pAudioDesc;
	libvlc_track_description_t *m_pSpuDesc;
	libvlc_event_manager_t *m_pEventManager;

	int m_nTracksCount;
	int m_nVideoWidth;
	int m_nVideoHeight;
	wstring m_wfile; 
	CCritSec m_csVideo;
	CCritSec m_csAudio;
	queue<unsigned char *>m_vq_t;
	queue<unsigned char *>m_vq;
	queue<AudioChunk *>m_aq;
	list<string> m_urlprexifes;
	int m_nAudioCount;
	int m_nSubtitleCount;
	float m_pFps;
	LONGLONG m_pts_start;
	BOOL m_bEos;
	CAMEvent m_evReceiveVideo;
	//CAMEvent m_evAudio;

	void ReleasePlayer();
	void ReleaseTracksInfo();

	void PopulateUrlPrefixes();
	BOOL IsLocalFile(string s);
	void QueryStreamInfo();
public:
	void *OnVideoLock(void **pixels);
	void OnVideoUnlock(void *id, void *const *pixels);
	void OnVideoDisplay(void *id);
	void OnAudioPlay(const void *samples, unsigned count, LONGLONG pts);
	void OnEndOfStream();
public:
	CVlcWrapper(void);
	~CVlcWrapper(void);
	HRESULT Load(WCHAR *file);
	HRESULT GetFileName(WCHAR *file);
	void Init();
	void Release();

	BOOL HasVideo();
	BOOL HasAudio();
	int GetVideoWidth();
	int GetVideoHeight();
	float GetVideoRate();
	
	int GetAudioTracksCount();
	int GetSubtitlesCount();
	int GetAudioTrack();
	int GetSubtitle();
	int SetAudioTrack(int id);
	int SetSubtitle(int id);
	void GetAudioTrackInfo(int number,int *id, WCHAR *name);
	void GetSubtitleInfo(int number,int *id, WCHAR *name);

	LONGLONG GetDuration();
	LONGLONG GetCurrentPosition();
    void SetCurrentPosition(LONGLONG position);

	void GetBlackFrame(unsigned char *p, int size, int realwidth, int realheight, int stride);
	BOOL GetVideoData(unsigned char *p, int size, int realwidth, int realheight, int stride);
	BOOL GetAudioData(unsigned char *p, int size, int *realsize, LONGLONG *rts, LONGLONG *rte);
	void ClearVideoQueue();
	void ClearAudioQueue();

	HRESULT Run();
	int GetState();

	HRESULT Pause();
	HRESULT Stop();

	BOOL IsEndOfStream();
};

