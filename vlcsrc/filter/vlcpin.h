#pragma once

class CVlcWrapper;
class CVlcPin : public CSourceStream, public CSourceSeeking
{
public:
	BOOL m_bDiscont;
	virtual void SeekStop();
	void SeekStart();

protected:
	CCritSec m_cSharedState;            // Protects our internal state
	CVlcWrapper *m_pVlc;
	

	void UpdateFromSeek();
	
	virtual HRESULT ChangeStart();
    virtual HRESULT ChangeStop();
    virtual HRESULT ChangeRate();
	STDMETHODIMP GetDuration(LONGLONG *pDuration);
	STDMETHODIMP SetPositions(LONGLONG *pCurrent,  DWORD CurrentFlags, LONGLONG *pStop,  DWORD StopFlags);

	DWORD ThreadProc();
	virtual HRESULT OnThreadStopPlay() {return NOERROR;};
public:
	CVlcPin(HRESULT *phr, CSource *pFilter, WCHAR *pName, CVlcWrapper *pVlc);
    ~CVlcPin(); 

	void OnSeek();
};
