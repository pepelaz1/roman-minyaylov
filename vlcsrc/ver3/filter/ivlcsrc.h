#ifndef __IVLCSRC__
#define __IVLCSRC__

#ifdef __cplusplus
extern "C" {
#endif

	// {77493EB7-6D00-41C5-9535-7C593824E892}
	DEFINE_GUID(IID_IVlcSrc, 
	0x77493eb7, 0x6d00, 0x41c5, 0x95, 0x35, 0x7c, 0x59, 0x38, 0x24, 0xe8, 0x92);


    DECLARE_INTERFACE_(IVlcSrc, IUnknown)
    {
		STDMETHOD(SetFile) (THIS_
					WCHAR *file
					) PURE;
		
		STDMETHOD(GetAudioTracksCount) (THIS_
					int *count
					) PURE;
		
		STDMETHOD(GetAudioTrackInfo) (THIS_
					int number,
					int *id, 
					WCHAR *name
					) PURE;

		STDMETHOD(GetAudioTrack) (THIS_
					int *id					
					) PURE;

		STDMETHOD(SetAudioTrack) (THIS_
					int id					
					) PURE;

		STDMETHOD(GetSubtitlesCount) (THIS_
					int *count
					) PURE;
		
		STDMETHOD(GetSubtitleInfo) (THIS_
					int number,
					int *id, 
					WCHAR *name
					) PURE;

		STDMETHOD(GetSubtitle) (THIS_
					int *id					
					) PURE;

		STDMETHOD(SetSubtitle) (THIS_
					int id					
					) PURE;
    };

#ifdef __cplusplus
}
#endif

#endif // __IVLCSRC__

