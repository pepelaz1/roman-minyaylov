#include "vlcwrapper.h"
#include <vlc/vlc.h>
#include <process.h>
#include <string>
#include "strhelper.h"
#include <codecvt>
using namespace std;


CVlcWrapper::CVlcWrapper(void) : 
	m_pPlayer(NULL),
	m_pEventManager(NULL),
	m_pMedia(NULL),
	m_pInstance(NULL),
	m_pTracksInfo(NULL),
	m_nTracksCount(0),
	m_nVideoWidth(0),
	m_nVideoHeight(0),
	m_nAudioCount(-1),
	m_nSubtitleCount(-1),
	m_pAudioDesc(NULL),
	m_pSpuDesc(NULL),
	m_bEos(FALSE)	
{
	PopulateUrlPrefixes();
}
	
CVlcWrapper::~CVlcWrapper(void)
{
	ClearVideoQueue();
	ClearAudioQueue();
	ReleaseTracksInfo();
	ReleasePlayer();
	Release();
}

void CVlcWrapper::ReleasePlayer()
{
	if (m_pAudioDesc)
	{
		libvlc_free(m_pAudioDesc);
		m_pAudioDesc = NULL;
	}

	if (m_pSpuDesc)
	{
		libvlc_free(m_pSpuDesc);
		m_pSpuDesc = NULL;
	}

	if (m_pMedia)
	{
		libvlc_media_release(m_pMedia);
		m_pMedia = NULL;
	}


	if (m_pPlayer)
	{
		libvlc_media_player_release (m_pPlayer);
		m_pPlayer = NULL;
	}
}

void CVlcWrapper::ReleaseTracksInfo()
{
	if (m_pTracksInfo)
	{
		libvlc_free(m_pTracksInfo);
		m_pTracksInfo = NULL;
		m_nTracksCount = 0;
	}
}

void CVlcWrapper::Release()
{
	ReleaseTracksInfo();
	ReleasePlayer();
	if (m_pInstance)
		libvlc_release(m_pInstance);
}

void CVlcWrapper::ClearVideoQueue()
{
	CAutoLock lck(&m_csVideo);
	while (m_vq.size () > 0)
	{
		unsigned char *buff = m_vq.front();
		m_vq.pop();
		delete buff;
	}
}

void CVlcWrapper::ClearAudioQueue()
{
	CAutoLock lck(&m_csAudio);
	while (m_aq.size () > 0)
	{
		AudioChunk *chunk = m_aq.front();
		delete chunk->buff;
		m_aq.pop();
		delete chunk;
	}
}

void CVlcWrapper::Init()
{
	m_pInstance = libvlc_new(0, NULL);
}


void aplay(void *ctx, const void *samples, unsigned count, LONGLONG pts)
{
	CVlcWrapper *vlc = (CVlcWrapper *)ctx;
	vlc->OnAudioPlay(samples,count,pts);
}


void CVlcWrapper::OnAudioPlay(const void *samples, unsigned count, LONGLONG pts)
{
	
	//CAutoLock lck(&m_csAudio);
//	__int64 t = libvlc_delay(pts);
//	if (t > 0)
//	{
		AudioChunk *chunk = new AudioChunk();
		int size = count*4;
		chunk->buff = new unsigned char[size];
		chunk->size = size;
		memcpy(chunk->buff,samples, size);
		chunk->pts = pts;
		m_aq.push(chunk);

		DWORD threadID = GetCurrentThreadId();

		if (m_pts_start == -1)
			m_pts_start = chunk->pts ;


		LONGLONG cpts = (chunk->pts - m_pts_start) * 10;
		char str[256];
		sprintf(str,"CVlcWrapper::OnAudioPlay, Audio queue size - %d, chunk->pts - %I64d, *pts - %I64d, thread id = %d\n",m_aq.size(), chunk->pts, cpts, threadID);
		OutputDebugStringA(str);
			
		chunk->pts = cpts;
	//}
}

BOOL CVlcWrapper::GetAudioData(unsigned char *p, int size, int *realsize, LONGLONG *rts, LONGLONG *rte)
{
//	CAutoLock lck(&m_csAudio);
	if (m_aq.size () > 1) // at least two 
	{



//		BOOL b = false;
//		AudioChunk *chunk;
//		while (m_aq.size() > 2)
//		for(;;)
//		{
//			
//			chunk = m_aq.front();
//			if (*realsize + chunk->size > size || m_aq.size() <= 2)
//			{
//				*rte = chunk->pts;
//				//delete chunk->buff;
//			//	delete chunk;
//				char str[256];
//				sprintf(str,"Audio queue size - %d, *realsize - %d, rts - %I64d, rte - %I64d\n", m_aq.size(), *realsize, *rts, *rte);
//				OutputDebugStringA(str);
//				return TRUE;
//			}
//
//			//memcpy(p,chunk->buff,chunk->size);
//			CopyMemory(p,chunk->buff,chunk->size);
//			
//			p += chunk->size;
//			*realsize += chunk->size;
//
//			if (!b)
//			{
//				*rts = chunk->pts;
//				b = true;
//			}
//			//if (!b)
//			//{
//			//__int64 t = libvlc_delay(chunk->pts);// * 10000.f / 90.0f;
//	//		if (m_pts_start == -1)
//		//		m_pts_start = chunk->pts;
//
//			//DWORD threadID = GetCurrentThreadId();
//
//			///*pts = (chunk->pts - m_pts_start) * 10;
//		//	*pts = chunk->pts;
////			char str[256];
////			sprintf(str,"Audio queue size - %d, chunk->pts - %I64d, *pts - %I64d, thread id = %d\n",m_aq.size(), chunk->pts, *pts, threadID);
////			OutputDebugStringA(str);
//
//				//b = true;
//			//}
//			m_aq.pop();
//
//			//delete buff;
//			delete chunk->buff;
//			delete chunk;
//		}
//
//		return TRUE;
		AudioChunk *chunk = m_aq.front();
		*rts = chunk->pts;
		CopyMemory(p,chunk->buff,chunk->size);
		*realsize = chunk->size;
		m_aq.pop();
		delete chunk->buff;
		delete chunk;
		chunk = m_aq.front();
		*rte = chunk->pts;

		char str[256];
		sprintf(str,"CVlcWrapper::GetAudioData, Audio queue size - %d, *realsize - %d, rts - %I64d, rte - %I64d\n", m_aq.size(), *realsize, *rts, *rte);
		OutputDebugStringA(str);
		return TRUE;

	}
	else
	{
		//Sleep(100);
		*rts = -1;
		*rte = -1;
		*realsize = 0;
	}
	return FALSE;
}


static void *vlock(void *ctx, void **pixels)
{
	CVlcWrapper *vlc = (CVlcWrapper *)ctx;
	return vlc->OnVideoLock(pixels);
}

void *CVlcWrapper::OnVideoLock(void **pixels)
{
	//CAutoLock lck(&m_csVideo);
	unsigned char *buff = new unsigned char[m_nVideoWidth * m_nVideoHeight * 2];
	m_vq.push(buff);
	*pixels = buff;
	return NULL;
}

static void vunlock(void *ctx, void *id, void *const *pixels)
{
	CVlcWrapper *vlc = (CVlcWrapper *)ctx;
	vlc->OnVideoUnlock(id,pixels);
}

void CVlcWrapper::OnVideoUnlock(void *id, void *const *pixels)
{
	//CAutoLock lck(&m_csVideo);
}

static void vdisplay(void *ctx, void *id)
{
}

BOOL CVlcWrapper::GetVideoData(unsigned char *p, int size)
{
	//CAutoLock lck(&m_csVideo);
	if (m_vq.size () > 1)
	{
		char str[256];
		sprintf(str,"Video queue size - %d\n",m_vq.size());
		//OutputDebugStringA(str);

		unsigned char *buff = m_vq.front();
		memcpy(p,buff,size);
		m_vq.pop();
		delete buff;
		return TRUE;
	}
	return FALSE;
}

void vlcEvent(const libvlc_event_t* evt, void *ctx)
{
	if( evt->type == libvlc_MediaPlayerEndReached )
	{
		CVlcWrapper *vlc = (CVlcWrapper *)ctx;
		vlc->OnEndOfStream();
	}
}
void CVlcWrapper::OnEndOfStream()
{
	m_bEos = TRUE;
}

BOOL CVlcWrapper::IsEndOfStream()
{
	return m_bEos;
}

void CVlcWrapper::PopulateUrlPrefixes()
{
		m_urlprexifes.push_back("http://");
	m_urlprexifes.push_back("rtsp://");
}

BOOL CVlcWrapper::IsLocalFile(string s)
{
	for( list<string>::iterator i = m_urlprexifes.begin();
		i !=  m_urlprexifes.begin(); i++)
	{
		if (s.find(*i) != -1)
			return FALSE;
	}
	return TRUE;
}

std::string wstring_to_utf8 (const std::wstring& str)
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
    return myconv.to_bytes(str);
}



HRESULT CVlcWrapper::Load(WCHAR *file)
{
	HRESULT hr = S_OK;

	wstring wfile = file;

	string sfile;
	sfile = wstring_to_utf8 (wfile);

	if (IsLocalFile(sfile))
		sfile = "file:///" + sfile;
	
	sfile = urlencode(sfile);
	try
	{
		ReleasePlayer();
		
		// create a new item
		//m = libvlc_media_new_path(inst, "file:///d://music/1.mp3");
		//m = libvlc_media_new_path(m_pInstance, "file:///D:/Work/Roman Minyaylov/vlcsrc/media/Sintel_DivXPlus_6500kbps.mkv");
		m_pMedia = libvlc_media_new_location(m_pInstance, sfile.c_str());
		//m_pMedia = libvlc_media_new_location(m_pInstance, "file:///D:/video/lag.mpg");

			
		m_pPlayer = libvlc_media_player_new_from_media(m_pMedia);
		m_pEventManager = libvlc_media_player_event_manager( m_pPlayer );
		libvlc_event_attach( m_pEventManager, libvlc_MediaPlayerEndReached, vlcEvent, this );

		// Set video callbacks
		libvlc_video_set_callbacks(m_pPlayer,vlock, vunlock, NULL, this);
		libvlc_video_set_format(m_pPlayer, "YUY2", 0, 0, 0);
		

			

		// Set audio callbacks
		libvlc_audio_set_callbacks(m_pPlayer, aplay, NULL, NULL, NULL, NULL, this);
		libvlc_audio_set_format(m_pPlayer,"",0, 0);


		//libvlc_media_parse(m_pMedia);
		// init media player
		libvlc_media_parse(m_pMedia);
		int n = libvlc_media_player_play(m_pPlayer);
	
		Sleep(1000);
		//Sleep(100);
		/*m_nVideoWidth = libvlc_video_get_width(m_pPlayer);
		while (!m_nVideoWidth)
		{
			m_nVideoWidth = libvlc_video_get_width(m_pPlayer);
			Sleep(10);
		}*/
		libvlc_media_player_stop(m_pPlayer);
	   
		
	

		ReleaseTracksInfo();
		m_nTracksCount = libvlc_media_get_tracks_info(m_pMedia, &m_pTracksInfo);
		if (!m_nTracksCount)
		{
			return E_FAIL;
		}

		m_nVideoWidth = libvlc_video_get_width(m_pPlayer);
		m_nVideoHeight = libvlc_video_get_height(m_pPlayer);

		ClearVideoQueue();		
	
		
		
		// Set video format
		libvlc_video_set_format(m_pPlayer, "YUY2", m_nVideoWidth, m_nVideoHeight,2 * m_nVideoWidth);
		
		// Set audio format
		libvlc_audio_set_format(m_pPlayer,"S16N", 48000, 2);
		
		//libvlc_media_player_play(m_pPlayer);

		m_nAudioCount = libvlc_audio_get_track_count(m_pPlayer);
		m_nSubtitleCount =  libvlc_video_get_spu_count(m_pPlayer);
		m_pAudioDesc = libvlc_audio_get_track_description (m_pPlayer);
		m_pSpuDesc = libvlc_video_get_spu_description(m_pPlayer);
		int duration = (int)libvlc_media_get_duration(m_pMedia);

		libvlc_media_player_set_position(m_pPlayer, 0);
		//libvlc_media_player_stop(m_pPlayer);

		
		int size = m_nVideoWidth * m_nVideoHeight * 2;
		for (int j = 0; j < 2; j++)
		{
			unsigned char *p = new unsigned char [size];
			for( int i = 0; i < size; i+=2 )
			{
				p[i] = 0x10;
				p[i+1] = 0x80;
			}
			m_vq.push(p);
		}

		m_bEos = FALSE;
	}
	catch(...)
	{
		hr = E_FAIL;
	}
	return hr;
}



BOOL CVlcWrapper::HasVideo()
{
	BOOL result = FALSE;
	for( int i = 0; i < m_nTracksCount; i++)
	{
		if (m_pTracksInfo[i].i_type == libvlc_track_video)
		{
			result = TRUE;
			break;
		}
	}
	return result;
}

BOOL CVlcWrapper::HasAudio()
{
	//return FALSE;

	BOOL result = FALSE;
	for( int i = 0; i < m_nTracksCount; i++)
	{
		if (m_pTracksInfo[i].i_type == libvlc_track_audio)
		{
			result = TRUE;
			break;
		}
	}
	return result;
}

int CVlcWrapper::GetVideoWidth()
{

	return m_nVideoWidth;
}

int CVlcWrapper::GetVideoHeight()
{
	return m_nVideoHeight;
}


int CVlcWrapper::GetAudioTracksCount()
{
	return m_nAudioCount;
}

int CVlcWrapper::GetSubtitlesCount()
{
	return m_nSubtitleCount;
}

int CVlcWrapper::GetAudioTrack()
{
	return libvlc_audio_get_track(m_pPlayer);
}
int CVlcWrapper::GetSubtitle()
{
	return libvlc_video_get_spu(m_pPlayer);
}

int CVlcWrapper::SetAudioTrack(int id)
{
	return libvlc_audio_set_track(m_pPlayer, id);
}

int CVlcWrapper::SetSubtitle(int id)
{
	return libvlc_video_set_spu(m_pPlayer, id);
}

void CVlcWrapper::GetAudioTrackInfo(int number, int *id, WCHAR *name)
{
	libvlc_track_description_t *p = m_pAudioDesc;
	for( int i = 0; i < number; i++)
		p = p->p_next;
	
	string s = p->psz_name;
	wstring ws(s.begin(), s.end());
		
	lstrcpy(name, ws.c_str());
	*id = p->i_id;
}

void CVlcWrapper::GetSubtitleInfo(int number, int *id, WCHAR *name)
{
	libvlc_track_description_t *p = m_pSpuDesc;
	for( int i = 0; i < number; i++)
		p = p->p_next;
	
	string s = p->psz_name;
	wstring ws(s.begin(), s.end());
		
	lstrcpy(name, ws.c_str());
	*id = p->i_id;
}


HRESULT CVlcWrapper::Run()
{
	HRESULT hr = S_OK;
	m_pts_start = -1;
	libvlc_media_player_play(m_pPlayer);
	return hr;
}

HRESULT CVlcWrapper::Pause()
{
	HRESULT hr = S_OK;
	libvlc_media_player_pause(m_pPlayer);
	return hr;
}


HRESULT CVlcWrapper::Stop()
{
	HRESULT hr = S_OK;
	libvlc_media_player_stop(m_pPlayer);
	ClearVideoQueue();
	ClearAudioQueue();
	return hr;
}

LONGLONG CVlcWrapper::GetDuration()
{
	if (!m_pMedia)
		return 0;

	float f = libvlc_media_player_get_position	(m_pPlayer	)	;

	libvlc_time_t dur =  libvlc_media_get_duration(m_pMedia) * 10000;
	return dur;
}

LONGLONG CVlcWrapper::GetCurrentPosition()
{
	if (!m_pMedia)
		return 0;

	libvlc_time_t dur =  libvlc_media_get_duration(m_pMedia) * 10000;
	float pos =  libvlc_media_player_get_position(m_pPlayer);

	return (LONGLONG)(dur * pos);
}

void CVlcWrapper::SetCurrentPosition(LONGLONG position)
{
	if (m_pMedia)
	{
		libvlc_time_t dur =  libvlc_media_get_duration(m_pMedia) * 10000;
		float pos =  position / (float)dur;
		libvlc_media_player_set_position(m_pPlayer, pos);
	}
}