#include <streams.h>
#include <dvdmedia.h>
#include <InitGuid.h>
#include "videopin.h"
#include "audiopin.h"
#include "filter.h"
#include "vlcwrapper.h"

CFilter::CFilter(IUnknown *pUnk, HRESULT *phr)
           : CSource(NAME("VlcSource"), pUnk, CLSID_VlcSource) 
{
	m_pVlc = new CVlcWrapper();
	m_pVlc->Init();
}


CFilter::~CFilter()
{	
	DeletePins();
	delete m_pVlc;
}


CUnknown * WINAPI CFilter::CreateInstance(IUnknown *pUnk, HRESULT *phr)
{
    CFilter *pNewFilter = new CFilter(pUnk, phr);

	if (phr)
	{
		if (pNewFilter == NULL) 
			*phr = E_OUTOFMEMORY;
		else
			*phr = S_OK;
	}
    return pNewFilter;
}

STDMETHODIMP CFilter::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
	if (riid == IID_IVlcSrc) {
        return GetInterface((IVlcSrc *) this, ppv);
	} else {
		return CSource::NonDelegatingQueryInterface(riid, ppv);
	}
}


HRESULT CFilter::CreatePins()
{
	HRESULT hr = S_OK;
	DeletePins();
	
	if (m_pVlc->HasVideo())
	{
		CVideoPin *pin = new CVideoPin(&hr, this, m_pVlc);
		if (FAILED(hr))
			return hr;
		m_pins.push_back(pin);
	}

	if (m_pVlc->HasAudio())
	{
		CAudioPin *pin = new CAudioPin(&hr, this, m_pVlc);
		if (FAILED(hr))
			return hr;
		m_pins.push_back(pin);
	}
	return S_OK;
}

void CFilter::DeletePins()
{
	for(list<CVlcPin *>::iterator i = m_pins.begin(); 
	  i != m_pins.end(); i++)
	{
		CVlcPin *pin = *i;
		if (pin)
			delete pin;
	}
	m_pins.clear();
}

STDMETHODIMP CFilter::Run(REFERENCE_TIME tStart)
{
	HRESULT hr = CSource::Run(tStart);
	if (FAILED(hr))
		return hr;

	//return hr;
	return m_pVlc->Run();
}

STDMETHODIMP CFilter::Pause()
{
	HRESULT hr = CSource::Pause();
	if (FAILED(hr))
		return hr;

	//return hr;
	return m_pVlc->Pause();
}

STDMETHODIMP CFilter::Stop()
{
	HRESULT hr = CSource::Stop();
	if (FAILED(hr))
		return hr;

	return m_pVlc->Stop();
}


//
// IVlcSrc implementation
//
STDMETHODIMP CFilter::SetFile(WCHAR *file)
{
	HRESULT hr = m_pVlc->Load(file);
	if (FAILED(hr))
		return hr;

	hr = CreatePins();
	if (FAILED(hr))
		return hr;

	return hr;
}

STDMETHODIMP CFilter::GetAudioTracksCount(int *count)
{
	if (!m_pVlc)
	{
		*count = -1;
		return S_FALSE;
	}
	*count = m_pVlc->GetAudioTracksCount();
	return S_OK;
}

STDMETHODIMP CFilter::GetAudioTrackInfo(int number, int *id, WCHAR *text)
{
	if (!m_pVlc)
		return S_FALSE;

	m_pVlc->GetAudioTrackInfo(number,id, text);
	return S_OK;
}

STDMETHODIMP CFilter::GetAudioTrack(int *id)
{
	if (!m_pVlc)
	{
		*id = -1;
		return S_FALSE;
	}
	*id = m_pVlc->GetAudioTrack();
	return S_OK;
}

STDMETHODIMP CFilter::SetAudioTrack(int id)
{
	if (!m_pVlc)
		return S_FALSE;

	if (m_pVlc->SetAudioTrack(id) == -1)
		return E_FAIL;

	return S_OK;
}

STDMETHODIMP CFilter::GetSubtitlesCount (int *count)
{
	if (!m_pVlc)
	{
		*count = -1;
		return S_FALSE;
	}
	*count = m_pVlc->GetSubtitlesCount();
	return S_OK;
}

STDMETHODIMP CFilter::GetSubtitleInfo (int number, int *id, WCHAR *text)
{
	if (!m_pVlc)
		return S_FALSE;
	
	m_pVlc->GetSubtitleInfo(number,id, text);
	return S_OK;
}

STDMETHODIMP CFilter::GetSubtitle(int *id)
{
	if (!m_pVlc)
	{
		*id = -1;
		return S_FALSE;
	}
	*id = m_pVlc->GetSubtitle();
	return S_OK;
}

STDMETHODIMP CFilter::SetSubtitle(int id)
{
	if (!m_pVlc)
		return S_FALSE;

	if (m_pVlc->SetSubtitle(id) == -1)
		return E_FAIL;
	
	return S_OK;
}
