#include <streams.h>
#include "vlcwrapper.h"
#include "vlcpin.h"



CVlcPin::CVlcPin(HRESULT *phr, CSource *pFilter, WCHAR *pName, CVlcWrapper *pVlc)
        : CSourceStream(NAME("Vlc source filter"), phr, pFilter,pName),
		CSourceSeeking(NAME("SeekVlc"), (IPin*)this, phr, &m_cSharedState),
		m_pVlc(pVlc)
{
}

CVlcPin::~CVlcPin()
{   
}

void CVlcPin::UpdateFromSeek()
{
    if (ThreadExists()) 
    {
        DeliverBeginFlush();
		m_pVlc->ClearVideoQueue();
		m_pVlc->ClearAudioQueue();
        // Shut down the thread and stop pushing data.
    //    m_pVlc->Stop();

		Stop();
		
        DeliverEndFlush();
        // Restart the thread and start pushing data again.
	//	m_pVlc->Pause();
        Pause();
    }
}


HRESULT CVlcPin::ChangeStart()
 {
	 UpdateFromSeek();
	 return S_OK;
 }

 HRESULT CVlcPin::ChangeStop()
 {
	  UpdateFromSeek();
	 return S_OK;
 }

 HRESULT CVlcPin::ChangeRate()
 {
	 return S_OK;
 }

 STDMETHODIMP CVlcPin::GetDuration(LONGLONG *pDuration)
 {
	 HRESULT hr = CSourceSeeking::GetDuration(pDuration);
	 if (FAILED(hr))
		 return hr;
	 *pDuration = m_pVlc->GetDuration();
	 return hr;
 }

STDMETHODIMP  CVlcPin::SetPositions(LONGLONG *pCurrent, DWORD CurrentFlags, LONGLONG *pStop,  DWORD StopFlags )
{
	HRESULT hr = CSourceSeeking::SetPositions(pCurrent, CurrentFlags, pStop, StopFlags);
	if (FAILED(hr))
		return hr;
	m_pVlc->SetCurrentPosition(*pCurrent);
	return hr;
}