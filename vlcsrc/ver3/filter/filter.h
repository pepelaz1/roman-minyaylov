#pragma once

#include "ivlcsrc.h"
#include <list>
using namespace std;

// {3FC97748-7CB6-4195-89DE-0717582A4863}
DEFINE_GUID(CLSID_VlcSource, 
0x3fc97748, 0x7cb6, 0x4195, 0x89, 0xde, 0x7, 0x17, 0x58, 0x2a, 0x48, 0x63);


class CVlcPin;
class CVlcWrapper;
class CFilter : public CSource, public IVlcSrc
{
private:
    CFilter(IUnknown *pUnk, HRESULT *phr);
    ~CFilter();

	list<CVlcPin *> m_pins;
	CVlcWrapper *m_pVlc;
	HRESULT CreatePins();
	void DeletePins();
public:
	DECLARE_IUNKNOWN;
    static CUnknown * WINAPI CreateInstance(IUnknown *pUnk, HRESULT *phr); 
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);
	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

	// IVlcSrc implementation
	STDMETHODIMP SetFile(WCHAR *file);
	STDMETHODIMP GetAudioTracksCount(int *count);
	STDMETHODIMP GetAudioTrackInfo(int number,int *id, WCHAR *name);
	STDMETHODIMP GetAudioTrack(int *id);
	STDMETHODIMP SetAudioTrack(int id);
	STDMETHODIMP GetSubtitlesCount (int *count);
	STDMETHODIMP GetSubtitleInfo (int number,int *id, WCHAR *name);
	STDMETHODIMP GetSubtitle(int *id);
	STDMETHODIMP SetSubtitle(int id);
};

