#include <streams.h>
#include "vlcwrapper.h"
#include "audiopin.h"



CAudioPin::CAudioPin(HRESULT *phr, CSource *pFilter, CVlcWrapper *pVlc)
       : CVlcPin( phr, pFilter, L"Audio", pVlc),
        m_frameno(0)
{
}

CAudioPin::~CAudioPin()
{   
}

//
// GetMediaType
//
HRESULT CAudioPin::GetMediaType(int iPosition, CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    if(iPosition < 0)
        return E_INVALIDARG;

    // Have we run off the end of types?
    if(iPosition > 0)
        return VFW_S_NO_MORE_ITEMS;

  
	WAVEFORMATEX *wfe = (WAVEFORMATEX*) pmt->AllocFormatBuffer(sizeof(WAVEFORMATEX));
	if (wfe == 0)
		 return(E_OUTOFMEMORY);

    ZeroMemory(wfe, sizeof(WAVEFORMATEX));
    wfe->wFormatTag = WAVE_FORMAT_PCM;
	wfe->nChannels = 2;
	wfe->nSamplesPerSec = 48000;
    wfe->wBitsPerSample = 16;
    wfe->nBlockAlign = 4;//m_pwfx->nChannels * m_pwfx->wBitsPerSample / 8;
    wfe->nAvgBytesPerSec = wfe->nSamplesPerSec *  wfe->nBlockAlign ;	 

    pmt->SetType(&MEDIATYPE_Audio);
    pmt->SetFormatType(&FORMAT_WaveFormatEx);
	pmt->SetSubtype(&MEDIASUBTYPE_PCM);
    pmt->SetTemporalCompression(FALSE);
    pmt->SetSampleSize(4);

    return NOERROR;

} 


//
// CheckMediaType
//
HRESULT CAudioPin::CheckMediaType(const CMediaType *pMediaType)
{
    CheckPointer(pMediaType,E_POINTER);

    if((*(pMediaType->Type()) != MEDIATYPE_Audio))          
    {                                                  
        return E_INVALIDARG;
    }

    // Check for the subtypes we support
    const GUID *SubType = pMediaType->Subtype();
    if (SubType == NULL)
        return E_INVALIDARG;

	if ( *SubType != MEDIASUBTYPE_PCM)
		return E_INVALIDARG;

    // Get the format area of the media type
    WAVEFORMATEX *wfe = (WAVEFORMATEX *)pMediaType->Format();
    if(wfe == NULL)
        return E_INVALIDARG;   

    return S_OK;  // This format is acceptable.

} // CheckMediaType


//
// DecideBufferSize
//
// This will always be called after the format has been sucessfully
// negotiated. So we have a look at m_mt to see what size image we agreed.
// Then we can ask for buffers of the correct size to contain them.
//
HRESULT CAudioPin::DecideBufferSize(IMemAllocator *pAlloc,
                                      ALLOCATOR_PROPERTIES *pProperties)
{
    CheckPointer(pAlloc,E_POINTER);
    CheckPointer(pProperties,E_POINTER);

    CAutoLock cAutoLock(m_pFilter->pStateLock());
    HRESULT hr = NOERROR;
	
	pProperties->cbBuffer		= 192000;
	pProperties->cBuffers		= 1;
	pProperties->cbAlign		= 1;		
	pProperties->cbPrefix		= 0;

    ASSERT(pProperties->cbBuffer);

    // Ask the allocator to reserve us some sample memory. NOTE: the function
    // can succeed (return NOERROR) but still not have allocated the
    // memory that we requested, so we must check we got whatever we wanted.
    ALLOCATOR_PROPERTIES Actual;
    hr = pAlloc->SetProperties(pProperties,&Actual);
    if(FAILED(hr))
    {
        return hr;
    }

    // Is this allocator unsuitable?
    if(Actual.cbBuffer < pProperties->cbBuffer)
    {
        return E_FAIL;
    }

    return NOERROR;

} // DecideBufferSize


//
// SetMediaType
//
// Called when a media type is agreed between filters
//
/*HRESULT CAudioPin::SetMediaType(const CMediaType *pMediaType)
{
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    // Pass the call up to my base class
    HRESULT hr = CSourceStream::SetMediaType(pMediaType);

    if(SUCCEEDED(hr))
    {
        VIDEOINFO * pvi = (VIDEOINFO *) m_mt.Format();
        if (pvi == NULL)
            return E_UNEXPECTED;

        switch(pvi->bmiHeader.biBitCount)
        {
            case 32:    // RGB32
                // Save the current media type and bit depth
                m_MediaType = *pMediaType;
            //    m_nCurrentBitDepth = pvi->bmiHeader.biBitCount;
                hr = S_OK;
                break;

            default:
                // We should never agree any other media types
                ASSERT(FALSE);
                hr = E_INVALIDARG;
                break;
        }
    } 

    return hr;

} // SetMediaType*/


HRESULT CAudioPin::OnThreadStartPlay()
{
	//m_frameno = 0;
	m_rtStart = 0;
	m_rtSync = 0;
	//m_pVlc->Run();
	return  DeliverNewSegment(m_rtStart, 0, 1.0);
}

//BOOL first = false;

REFERENCE_TIME CAudioPin::GetSyncTime()
{
	return m_rtSync;
}


HRESULT CAudioPin::FillBuffer(IMediaSample *pSample)
{
	//char str[256];
	//sprintf(str,"FillBuffer\n");
	//OutputDebugStringA(str);


	BYTE *pData;
    long cbData;

    CheckPointer(pSample, E_POINTER);

    CAutoLock cAutoLockShared(&m_cSharedState);

	if (m_pVlc->IsEndOfStream())
		return S_FALSE;

    // Access the sample's data buffer
    pSample->GetPointer(&pData);
    cbData = pSample->GetSize();


	REFERENCE_TIME rt = 0;
	int size = 0;
	//BOOL b = m_pVlc->GetAudioData(pData,cbData, &size, &rt);
	//while(!size)
	//{
	LONGLONG rts = 0, rte = 0;
	
	for(;;)
	{
		m_pVlc->Run();
		m_pVlc->GetAudioData(pData,cbData, &size, &rts, &rte);
		if (size)
			break;
		Sleep(100);
	}

	//m_rtSync = rts;
	//rts = 0;
	//rte = 0;
	//pSample->SetTime(NULL, NULL);
	//	Sleep(100);
	//}

    //m_rtStart = rt;
	//LONGLONG dur = ((LONGLONG)(size/4)*10000000)/48000;
	//REFERENCE_TIME rtStop = m_rtStart + dur;
	//REFERENCE_TIME rtStop = m_rtStart + rt;

	//pSample->SetTime(&m_rtStart, &rtStop);

	//m_rtStart += rt;
	//m_rtStart += dur;
	
//	if (size > 0)

	pSample->SetTime(&rte, &rts);
	pSample->SetActualDataLength(size);
	pSample->SetSyncPoint(TRUE);
	//}

//	int n = m_timeline.GetSlideNo(rtStart);
	//m_painter.Draw( m_doc.GetSlide(n), m_frameno, rtStart, pData, cbData);
//
//    pSample->SetTime(&rtStart, &rtStop); 
//	m_frameno++;

	// Set TRUE on every sample for uncompressed frames
   

	char str[MAX_PATH];
	sprintf(str,"CAudioPin::FillBuffer: rtStart=%I64d, rtStop=%I64d\n", rts, rte);
	OutputDebugStringA(str);

    return S_OK;
}
