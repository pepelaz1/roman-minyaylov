#include <streams.h>
#include "vlcwrapper.h"
#include "audiopin.h"
#include "videopin.h"



CVideoPin::CVideoPin(HRESULT *phr, CSource *pFilter, CVlcWrapper *pVlc)
		: CVlcPin(phr, pFilter, L"Video", pVlc)//,
      //  m_frameno(0),
		//m_pPrevFrame(NULL)

{
//	m_rtFrameLength = (REFERENCE_TIME)(UNITS / m_pVlc->GetVideoRate());
}

CVideoPin::~CVideoPin()
{   
	//if (m_pPrevFrame)
	//{
	//	delete m_pPrevFrame;
	//	m_pPrevFrame = NULL;
	//}
}

STDMETHODIMP CVideoPin::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IMediaSeeking) 
	{
		return GetInterface((IMediaSeeking *)this, ppv);
	} 
	else {
		return CBaseOutputPin::NonDelegatingQueryInterface(riid, ppv);
	}
}


HRESULT CVideoPin::GetMediaType(int iPosition, CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    if(iPosition < 0)
        return E_INVALIDARG;

    // Have we run off the end of types?
    if(iPosition > 0)
        return VFW_S_NO_MORE_ITEMS;

    VIDEOINFO *pvi = (VIDEOINFO *) pmt->AllocFormatBuffer(sizeof(VIDEOINFO));
    if(NULL == pvi)
        return(E_OUTOFMEMORY);

    // Initialize the VideoInfo structure before configuring its members
    ZeroMemory(pvi, sizeof(VIDEOINFO));

    switch(iPosition)
    {
        case 0:
        {    
         //   pvi->bmiHeader.biCompression = BI_RGB;
         //  pvi->bmiHeader.biBitCount    = 32;
			pvi->bmiHeader.biCompression = MAKEFOURCC('Y','U','Y','2');
			pvi->bmiHeader.biBitCount    = 16;
            break;
        }
    }

    // Adjust the parameters common to all formats
    pvi->bmiHeader.biSize       = sizeof(BITMAPINFOHEADER);
    pvi->bmiHeader.biWidth      = m_pVlc->GetVideoWidth();
    pvi->bmiHeader.biHeight     = m_pVlc->GetVideoHeight();
    pvi->bmiHeader.biPlanes     = 1;
    pvi->bmiHeader.biSizeImage  = GetBitmapSize(&pvi->bmiHeader);
    pvi->bmiHeader.biClrImportant = 0;

    SetRectEmpty(&(pvi->rcSource)); // we want the whole image area rendered.
    SetRectEmpty(&(pvi->rcTarget)); // no particular destination rectangle

    pmt->SetType(&MEDIATYPE_Video);
    pmt->SetFormatType(&FORMAT_VideoInfo);
    pmt->SetTemporalCompression(FALSE);

    // Work out the GUID for the subtype from the header info.
    const GUID SubTypeGUID = GetBitmapSubtype(&pvi->bmiHeader);
    pmt->SetSubtype(&SubTypeGUID);
    pmt->SetSampleSize(pvi->bmiHeader.biSizeImage);

    return NOERROR;

}


HRESULT CVideoPin::CheckMediaType(const CMediaType *pMediaType)
{
    CheckPointer(pMediaType,E_POINTER);

    if((*(pMediaType->Type()) != MEDIATYPE_Video) ||   // we only output video
        !(pMediaType->IsFixedSize()))                  // in fixed size samples
    {                                                  
        return E_INVALIDARG;
    }

    // Check for the subtypes we support
    const GUID *SubType = pMediaType->Subtype();
    if (SubType == NULL)
        return E_INVALIDARG;

	if ( *SubType != MEDIASUBTYPE_YUY2)
	{
		return E_INVALIDARG;
	}


    // Get the format area of the media type
    VIDEOINFO *pvi = (VIDEOINFO *) pMediaType->Format();

    if(pvi == NULL)
        return E_INVALIDARG;

    // Check if the image width & height have changed
	if(pvi->bmiHeader.biWidth != m_pVlc->GetVideoWidth() || pvi->bmiHeader.biHeight != m_pVlc->GetVideoHeight() )
    {
        // If the image width/height is changed, fail CheckMediaType() to force
        // the renderer to resize the image.
        return E_INVALIDARG;
    }

    // Don't accept formats with negative height, which would cause the desktop
    // image to be displayed upside down.
    if (pvi->bmiHeader.biHeight < 0)
        return E_INVALIDARG;

    return S_OK;  // This format is acceptable.

} // CheckMediaType


//
// DecideBufferSize
//
// This will always be called after the format has been sucessfully
// negotiated. So we have a look at m_mt to see what size image we agreed.
// Then we can ask for buffers of the correct size to contain them.
//
HRESULT CVideoPin::DecideBufferSize(IMemAllocator *pAlloc,
                                      ALLOCATOR_PROPERTIES *pProperties)
{
    CheckPointer(pAlloc,E_POINTER);
    CheckPointer(pProperties,E_POINTER);

    CAutoLock cAutoLock(m_pFilter->pStateLock());
    HRESULT hr = NOERROR;

    VIDEOINFO *pvi = (VIDEOINFO *) m_mt.Format();
    pProperties->cBuffers = 1;
    pProperties->cbBuffer = pvi->bmiHeader.biSizeImage;

    ASSERT(pProperties->cbBuffer);

    // Ask the allocator to reserve us some sample memory. NOTE: the function
    // can succeed (return NOERROR) but still not have allocated the
    // memory that we requested, so we must check we got whatever we wanted.
    ALLOCATOR_PROPERTIES Actual;
    hr = pAlloc->SetProperties(pProperties,&Actual);
    if(FAILED(hr))
    {
        return hr;
    }

    // Is this allocator unsuitable?
    if(Actual.cbBuffer < pProperties->cbBuffer)
    {
        return E_FAIL;
    }

    // Make sure that we have only 1 buffer (we erase the ball in the
    // old buffer to save having to zero a 200k+ buffer every time
    // we draw a frame)
    ASSERT(Actual.cBuffers == 1);
    return NOERROR;

} // DecideBufferSize


//
// SetMediaType
//
// Called when a media type is agreed between filters
//
HRESULT CVideoPin::SetMediaType(const CMediaType *pMediaType)
{
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    // Pass the call up to my base class
    HRESULT hr = CSourceStream::SetMediaType(pMediaType);

    if(SUCCEEDED(hr))
    {
        VIDEOINFO * pvi = (VIDEOINFO *) m_mt.Format();
        if (pvi == NULL)
            return E_UNEXPECTED;

        switch(pvi->bmiHeader.biBitCount)
        {
            case 16:    // YUY2
                // Save the current media type and bit depth
                m_MediaType = *pMediaType;
            //    m_nCurrentBitDepth = pvi->bmiHeader.biBitCount;
                hr = S_OK;
                break;

            default:
                // We should never agree any other media types
                ASSERT(FALSE);
                hr = E_INVALIDARG;
                break;
        }
    } 

    return hr;

} // SetMediaType


HRESULT CVideoPin::OnThreadStartPlay()
{
//	m_frameno = 0;
	
//	if (m_pPrevFrame)
//	{
//		delete m_pPrevFrame;
//		m_pPrevFrame = NULL;
//	}

	//int size = m_pVlc->GetVideoWidth() * m_pVlc->GetVideoHeight() * 2;
	//m_pPrevFrame = new unsigned char [size];
	//for( int i = 0; i < size; i+=2 )
	//{
//		m_pPrevFrame[i] = 0x10;
		//m_pPrevFrame[i+1] = 0x80;
	//}
	
	//m_frameno = 10;
	//m_rtStart = m_frameno * m_rtFrameLength;
//	m_bDiscontinuity = TRUE;
    return DeliverNewSegment(m_rtStart, 0, 1.0);

}

// This is where we insert the DIB bits into the video stream.
// FillBuffer is called once for every sample in the stream.
HRESULT CVideoPin::FillBuffer(IMediaSample *pSample)
{
	BYTE *pData;
	long cbData;

	CheckPointer(pSample, E_POINTER);

	CAutoLock cAutoLockShared(&m_cSharedState);

	// Access the sample's data buffer
	pSample->GetPointer(&pData);
	cbData = pSample->GetSize();


	HRESULT hr = S_OK;
	REFERENCE_TIME rtStart = 0, rtStop = 0, rtSync = 0;
	//for(;;)
	//{
	//	rtStart = m_frameno * m_rtFrameLength;
	//	rtStop  = rtStart + m_rtFrameLength;

	//CRefTime streamTime;
	//hr = m_pFilter->StreamTime(streamTime);
	//REFERENCE_TIME rtSync;
	//	CAudioPin *apin = (CAudioPin *)m_pFilter->GetPin(1);
	//	rtSync = apin->GetSyncTime();
	//


	BOOL b = m_pVlc->GetVideoData(pData, cbData);
	m_frameno++;

	if (m_pVlc->IsEndOfStream() && !b)
		return S_FALSE;



	pSample->SetTime(&rtStart, &rtStop); 
	m_frameno++;

	// Set TRUE on every sample for uncompressed frames
	// pSample->SetSyncPoint(TRUE);

	//char str[256];
	//sprintf(str,"CVideoPin::FillBuffer: rtStart=%I64d, rtStop=%I64d, rtSync=%I64d, m_frameno=%d\n", rtStart, rtStop, rtSync, m_frameno);
	//sprintf(str,"CVideoPin::FillBuffer: rtStart=%I64d, rtStop=%I64d, m_frameno=%d, b = %d\n", rtStart, rtStop, m_frameno, b);
	//OutputDebugStringA(str);


	return S_OK;
}



 
