
// sampledlg.cpp : implementation file
//

#include "stdafx.h"
#include "sample.h"
#include "sampledlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

 #pragma warning( disable : 4995)

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSampleDlg dialog

CSampleDlg::CSampleDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSampleDlg::IDD, pParent)

{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PLAY_BTN, m_btnPlay);
	DDX_Control(pDX, IDC_PAUSE_BTN, m_btnPause);
	DDX_Control(pDX, IDC_STOP_BTN, m_btnStop);
	DDX_Control(pDX, IDC_FILE_STATIC, m_stcMedia);
	DDX_Control(pDX, IDC_FILE_EDIT, m_edtFile);
	DDX_Control(pDX, IDC_BROWSE_BTN, m_btnBrowse);
	DDX_Control(pDX, IDC_SLIDER1, m_slider);
	DDX_Control(pDX, IDC_AUDIO_STATIC, m_stcAudio);
	DDX_Control(pDX, IDC_AUDIO_COMBO, m_cmbAudio);
	DDX_Control(pDX, IDC_SUBTITLE_STATIC, m_stcSubtitle);
	DDX_Control(pDX, IDC_SUBTITLE_COMBO, m_cmbSubtitle);
}

BEGIN_MESSAGE_MAP(CSampleDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_PLAY_BTN, &CSampleDlg::OnBnClickedPlayBtn)
	ON_BN_CLICKED(IDC_PAUSE_BTN, &CSampleDlg::OnBnClickedPauseBtn)
	ON_BN_CLICKED(IDC_STOP_BTN, &CSampleDlg::OnBnClickedStopBtn)
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_BN_CLICKED(IDC_SET_BTN, &CSampleDlg::OnBnClickedSetBtn)
	ON_BN_CLICKED(IDC_BROWSE_BTN, &CSampleDlg::OnBnClickedBrowseBtn)
	ON_WM_CLOSE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_TIMER()
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER1, &CSampleDlg::OnNMReleasedcaptureSlider1)
	ON_CBN_SELCHANGE(IDC_AUDIO_COMBO, &CSampleDlg::OnCbnSelchangeAudioCombo)
	ON_CBN_SELCHANGE(IDC_SUBTITLE_COMBO, &CSampleDlg::OnCbnSelchangeSubtitleCombo)
END_MESSAGE_MAP()


// CSampleDlg message handlers

BOOL CSampleDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CoInitialize(NULL);
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	CRect clrect;
	GetClientRect(&clrect);
	m_playerWnd.Create(_T("STATIC"), L"", WS_CHILD | WS_VISIBLE,
		CRect(clrect.left+10, clrect.top+10, clrect.right-10, clrect.bottom-60), this, 1234);


	m_btnPlay.EnableWindow(TRUE);
	m_btnPause.EnableWindow(FALSE);
	m_btnStop.EnableWindow(FALSE);
	m_edtFile.EnableWindow(TRUE);
	m_btnBrowse.EnableWindow(TRUE);

	//InitPlayer();
	m_slider.SetRange(0,100);
	SetTimer(1,1000, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSampleDlg::InitPlayer()
{
	CString filename;
	GetDlgItemText(IDC_FILE_EDIT, filename);
	m_player.SetFile(filename.GetBuffer());
	HRESULT hr = m_player.Init(m_playerWnd.m_hWnd);
	if (FAILED(hr))
	{
		CString errmsg = L"Failed to open file ";
		errmsg.Append(filename);
		MessageBox(errmsg.GetBuffer(),L"Error", MB_ICONERROR);
	}
	PopulateComboboxes();
}

void CSampleDlg::PopulateComboboxes()
{
	m_cmbAudio.Clear();
	for( int i = 0; i < m_player.GetAudioTracksCount(); i++)
	{
		WCHAR text[MAX_PATH];
		ZeroMemory(text,MAX_PATH);
		int id = 0;
		m_player.GetAudioTrackInfo(i, &id, text);
		if (text[0] != 0)
		{
			m_cmbAudio.InsertString(i, text);
			m_cmbAudio.SetItemData(i, id);
		}
	}
	m_cmbAudio.EnableWindow(FALSE);

	m_cmbSubtitle.Clear();
	for( int i = 0; i < m_player.GetSubtitlesCount(); i++)
	{
		WCHAR text[MAX_PATH];
		ZeroMemory(text,MAX_PATH);
		int id = 0;
		m_player.GetSubtitleInfo(i, &id, text);
		if (text[0] != 0)
		{
			m_cmbSubtitle.InsertString(i, text);
			m_cmbSubtitle.SetItemData(i, id);
		}
	}
	m_cmbSubtitle.EnableWindow(FALSE);
}

void CSampleDlg::UpdateComboboxes()
{
	m_cmbAudio.EnableWindow(TRUE);

	int curraudio = m_player.GetAudioTrack();
	for( int i = 0; i < m_cmbAudio.GetCount(); i++)
	{
		int id = m_cmbAudio.GetItemData(i);
		if (id == curraudio)
			m_cmbAudio.SetCurSel(i);
	}

	m_cmbSubtitle.EnableWindow(TRUE);

	int currsubtitle = m_player.GetSubtitle();
	for( int i = 0; i < m_cmbSubtitle.GetCount(); i++)
	{
		int id = m_cmbSubtitle.GetItemData(i);
		if (id == currsubtitle)
			m_cmbSubtitle.SetCurSel(i);
	}
}


void CSampleDlg::OnCbnSelchangeAudioCombo()
{
	int id = m_cmbAudio.GetItemData( m_cmbAudio.GetCurSel() );
	if ( m_player.SetAudioTrack(id) != 0)
	{
		WCHAR str[MAX_PATH];
		wsprintf(str, L"Failed to set audio track id=%d", id);
		MessageBox(str,L"Error", MB_ICONERROR);
	}
}


void CSampleDlg::OnCbnSelchangeSubtitleCombo()
{
	int id = m_cmbSubtitle.GetItemData( m_cmbSubtitle.GetCurSel() );
	if ( m_player.SetSubtitle(id) != 0)
	{
		WCHAR str[MAX_PATH];
		wsprintf(str, L"Failed to set subtitle id=%d", id);
		MessageBox(str,L"Error", MB_ICONERROR);
	}
}


void CSampleDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}


void CSampleDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		m_player.RepaintVW();
	}
}


HCURSOR CSampleDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

HBRUSH CSampleDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}


void CSampleDlg::OnBnClickedPlayBtn()
{
	m_player.Play();
	m_btnPlay.EnableWindow(FALSE);
	m_btnPause.EnableWindow(TRUE);
	m_btnStop.EnableWindow(TRUE);
	m_edtFile.EnableWindow(FALSE);
	m_btnBrowse.EnableWindow(FALSE);
	UpdateComboboxes();
}


void CSampleDlg::OnBnClickedPauseBtn()
{
	m_player.Pause();
	m_btnPlay.EnableWindow(TRUE);
	m_btnPause.EnableWindow(FALSE);
	m_btnStop.EnableWindow(TRUE);
	m_edtFile.EnableWindow(FALSE);
	m_btnBrowse.EnableWindow(FALSE);
}



void CSampleDlg::OnBnClickedStopBtn()
{
	m_player.Stop();
	m_btnPlay.EnableWindow(TRUE);
	m_btnPause.EnableWindow(FALSE);
	m_btnStop.EnableWindow(FALSE);
	m_edtFile.EnableWindow(TRUE);
	m_btnBrowse.EnableWindow(TRUE);
}


void CSampleDlg::OnBnClickedSetBtn()
{
	InitPlayer();
}

void CSampleDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
	MoveControls();	
}


void CSampleDlg::MoveControls()
{
	CRect r;
	GetClientRect(&r);

	if (m_playerWnd.m_hWnd)
		m_playerWnd.MoveWindow(r.left+10, r.top+10, r.right-20, r.bottom-70);
	m_player.UpdateVWPos();

	CRect r1;
	if (m_btnPlay.m_hWnd != NULL)
	{
		m_btnPlay.GetClientRect(&r1);
		r1.MoveToY(r.bottom-52);
		r1.MoveToX(11);
		m_btnPlay.MoveWindow(&r1);
	}

	if (m_btnPause.m_hWnd != NULL)
	{
		m_btnPause.GetClientRect(&r1);
		r1.MoveToY(r.bottom-52);
		r1.MoveToX(92);
		m_btnPause.MoveWindow(&r1);
	}

	if (m_btnStop.m_hWnd != NULL)
	{
		m_btnStop.GetClientRect(&r1);
		r1.MoveToY(r.bottom-52);
		r1.MoveToX(173);
		m_btnStop.MoveWindow(&r1);
	}


	if ( m_stcMedia.m_hWnd)
	{
		m_stcMedia.GetClientRect(&r1);
		r1.SetRect(12,r.bottom - 20, 100, r.bottom-5);
		m_stcMedia.MoveWindow(&r1);
	}

	int nn = 350;
	if (m_edtFile.m_hWnd)
	{
		m_edtFile.GetClientRect(&r1);
		r1.SetRect(60,r.bottom - 24, r.right-nn, r.bottom-4);
		m_edtFile.MoveWindow(&r1);
	}	

	if (m_btnBrowse.m_hWnd)
	{
		m_btnBrowse.GetClientRect(&r1);
		r1.SetRect(r.right-nn-1,r.bottom - 24, r.right-nn-1+25, r.bottom-4);
		m_btnBrowse.MoveWindow(&r1);
	}

	nn = 2;
	if (m_slider.m_hWnd)
	{
		m_slider.GetClientRect(&r1);
		r1.SetRect(260,r.bottom - 53, r.right-nn, r.bottom-28);
		m_slider.MoveWindow(&r1);
	}

	nn = 310;
	if ( m_stcAudio.m_hWnd)
	{
		m_stcAudio.GetClientRect(&r1);
		r1.SetRect(r.right-nn,r.bottom - 20, r.right-nn+40, r.bottom-5);
		m_stcAudio.MoveWindow(&r1);
	}

	nn = 280;
	if ( m_cmbAudio.m_hWnd)
	{
		m_cmbAudio.GetClientRect(&r1);
		r1.SetRect(r.right-nn,r.bottom - 24, r.right-nn+125, r.bottom-4);
		m_cmbAudio.MoveWindow(&r1);
	}

	nn = 140;
	if ( m_stcSubtitle.m_hWnd)
	{
		m_stcSubtitle.GetClientRect(&r1);
		r1.SetRect(r.right-nn,r.bottom - 20, r.right-nn+40, r.bottom-5);
		m_stcSubtitle.MoveWindow(&r1);
	}

	nn = 100;
	if ( m_cmbSubtitle.m_hWnd)
	{
		m_cmbSubtitle.GetClientRect(&r1);
		r1.SetRect(r.right-nn,r.bottom - 24, r.right-nn+90, r.bottom-4);
		m_cmbSubtitle.MoveWindow(&r1);
	}


	RedrawWindow();
}

void CSampleDlg::Finalize()
{
	m_player.Reset();
}

void CSampleDlg::OnMove(int x, int y)
{
	CDialogEx::OnMove(x, y);
	m_player.UpdateVWPos();
}

void CSampleDlg::OnBnClickedBrowseBtn()
{
	WCHAR filter[] = { L"All Files (*.*)|*.*||" };

	CFileDialog dlg(TRUE, L"", NULL, 0, filter);

	if( dlg.DoModal() == IDOK )
	{
		m_edtFile.SetWindowText(dlg.GetFolderPath() + "\\"+ dlg.GetFileName());
		InitPlayer();
	}
}


void CSampleDlg::OnClose()
{
	m_player.Reset();
	CDialogEx::OnClose();
}


void CSampleDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	CDialogEx::OnLButtonUp(nFlags, point);
}


void CSampleDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	CDialogEx::OnRButtonUp(nFlags, point);
}

void CSampleDlg::OnTimer(UINT_PTR nTimerID)
{
	LONGLONG duration = m_player.GetDuration();
	if (duration > 0 )
	{
		LONGLONG position = m_player.GetPosition();
		m_slider.SetPos( position * 100.0f / duration );
	}
}

void CSampleDlg::OnNMReleasedcaptureSlider1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LONGLONG duration = m_player.GetDuration();
	if (duration > 0 )
	{
		int pos = m_slider.GetPos();
		m_player.SetPosition( pos *  duration / 100.0f);
	}
	*pResult = 0;
}

