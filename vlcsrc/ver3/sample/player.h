#pragma once

#include "..\filter\ivlcsrc.h"
#include <evr.h>


using namespace std;
class CPlayer
{
private:
	HWND m_owner;
	WCHAR m_file[MAX_PATH];
	DWORD m_rot;

	CComPtr<IGraphBuilder>m_gb;
	CComPtr<IBaseFilter>m_vlcsrc;
	CComPtr<IVlcSrc>m_ivlc;
	CComPtr<IBaseFilter>m_evr;
	CComPtr<IMFGetService> m_gs;
	CComPtr<IMFVideoDisplayControl> m_vdc;

	CComPtr<IMediaControl>m_mc;
	CComPtr<IMediaSeeking>m_ms;
	CComPtr<IVideoWindow>m_vw;
	
	
	HRESULT AddFilter(const GUID &guid, LPWSTR name, CComPtr<IBaseFilter>&filter);
	HRESULT AddToRot(IUnknown *graph, DWORD *reg);
	void RemoveFromRot(DWORD reg);
	void TuneVideoWindow();
public:
	CPlayer();
	~CPlayer();

	HRESULT Init(HWND owner);
	void Reset();

	HRESULT Play();
	HRESULT Pause();
	HRESULT Stop();
	void UpdateVWPos();
	void RepaintVW();
	void SetFile(WCHAR *file);
	LONGLONG GetDuration();
	LONGLONG GetPosition();
	void SetPosition(LONGLONG position);
	int GetAudioTracksCount();
	int GetSubtitlesCount();
	void GetAudioTrackInfo(int idx, int *id, WCHAR *text);
	void GetSubtitleInfo(int idx, int *id, WCHAR *text);
	int GetAudioTrack();
	int GetSubtitle();
	int SetAudioTrack(int id);
	int SetSubtitle(int id);
};

