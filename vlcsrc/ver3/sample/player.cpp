#include "stdafx.h"

#pragma warning( disable : 4995)

// {3FC97748-7CB6-4195-89DE-0717582A4863}
DEFINE_GUID(CLSID_VlcSource, 
0x3fc97748, 0x7cb6, 0x4195, 0x89, 0xde, 0x7, 0x17, 0x58, 0x2a, 0x48, 0x63);


CPlayer::CPlayer() :
	m_owner(NULL)//,
	//m_Width(1024),
	//m_Height(768),
	//m_fps(30),
	//m_showstat(TRUE)
{
	ZeroMemory(m_file, MAX_PATH);
}

CPlayer::~CPlayer()
{
}

HRESULT CPlayer::Init(HWND owner)
{
	HRESULT hr = S_OK;
	Reset();
	m_owner = owner;
		
	hr = m_gb.CoCreateInstance(CLSID_FilterGraph);
	if (FAILED(hr))
		return hr;
	
	hr = AddFilter(CLSID_VlcSource, L"Vlc Source", m_vlcsrc); 
	if (FAILED(hr))
		return hr;
		
	hr = m_vlcsrc->QueryInterface(IID_IVlcSrc, (void **)&m_ivlc);
	if (FAILED(hr))
		return hr;

	hr = m_ivlc->SetFile(m_file);
	if (FAILED(hr))
		return hr;

	hr = AddFilter(CLSID_EnhancedVideoRenderer, L"Enchaced Video Renderer", m_evr); 
	if (FAILED(hr))
		return hr;


	CComPtr<IEnumPins>enm;
	hr = m_vlcsrc->EnumPins(&enm);
	if (FAILED(hr))
		return hr;

	CComPtr<IPin>pin;
	ULONG fetched = 0;
	while (enm->Next(1,&pin,&fetched) == S_OK)
	{
		hr = m_gb->Render(pin);
		if (FAILED(hr))
			return hr;
		pin.Release();
	}
	
	AddToRot(m_gb,&m_rot);

	hr = m_gb.QueryInterface(&m_mc);
	if (FAILED(hr))
		return hr;

	hr = m_gb.QueryInterface(&m_ms);
	if (FAILED(hr))
		return hr;
	
	hr = m_evr.QueryInterface(&m_gs);
	if (FAILED(hr))
		return hr;

	hr = m_gs->GetService(MR_VIDEO_RENDER_SERVICE, IID_IMFVideoDisplayControl, (void **)&m_vdc);
	if (FAILED(hr))
		return hr;


	TuneVideoWindow();
	return hr;
}

void CPlayer::TuneVideoWindow()
{
	m_vdc->SetVideoWindow(m_owner);
	UpdateVWPos();
}

void CPlayer::UpdateVWPos()
{

	if (m_vdc)
	{	
		RECT r;
		GetClientRect(m_owner, &r);
		m_vdc->SetVideoPosition(NULL,&r);
	}
}

void CPlayer::RepaintVW()
{
	if (m_vdc)
	{
		m_vdc->RepaintVideo();
	}
}

void CPlayer::Reset()
{
	RemoveFromRot(m_rot);
	if (m_mc)
		m_mc.Release();

	if (m_ms)
		m_ms.Release();

	if (m_gs)
		m_gs.Release();

	if (m_vdc)
		m_vdc.Release();

	if (m_ivlc)
		m_ivlc.Release();


	if (m_vlcsrc)
		m_vlcsrc.Release();

	if (m_evr)
		m_evr.Release();

	if (m_gb)
		m_gb.Release();
}

HRESULT CPlayer::Play()
{
	HRESULT hr = S_OK;
	if (!m_mc)
		return S_FALSE;

	hr = m_mc->Run();
	if (FAILED(hr))
		return hr;
	return hr;
}

HRESULT CPlayer::Pause()
{
	HRESULT hr = S_OK;
	if (!m_mc)
		return S_FALSE;

	hr = m_mc->Pause();
	if (FAILED(hr))
		return hr;
	return hr;
}

HRESULT CPlayer::Stop()
{
	HRESULT hr = S_OK;
	if (!m_mc)
		return S_FALSE;

	hr = m_mc->Stop();
	if (FAILED(hr))
		return hr;
	return hr;
}


HRESULT CPlayer::AddFilter(const GUID &guid, LPWSTR name, CComPtr<IBaseFilter> &filter)
{
	HRESULT hr = S_OK;
	CComPtr<IBaseFilter> flt;
	hr = flt.CoCreateInstance(guid);
	if (FAILED(hr))
		return hr;

	filter = flt;

	hr = m_gb->AddFilter(filter, name);
	if (FAILED(hr))
		return hr;	
	
	return hr;
}

HRESULT CPlayer::AddToRot(IUnknown *graph, DWORD *reg) 
{
    CComPtr<IMoniker> moniker = NULL;
    CComPtr<IRunningObjectTable>rot = NULL;

    if (FAILED(GetRunningObjectTable(0, &rot))) 
    {
        return E_FAIL;
    }
    
    const size_t STRING_LENGTH = 256;

    WCHAR wsz[STRING_LENGTH];
    StringCchPrintfW(wsz, STRING_LENGTH, L"FilterGraph %08x pid %08x", (DWORD_PTR)graph, GetCurrentProcessId());
    
    HRESULT hr = CreateItemMoniker(L"!", wsz, &moniker);
    if (SUCCEEDED(hr)) 
    {
        hr = rot->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, 
							graph, 
							moniker, 
							reg);
    }
    return hr;
}  

void CPlayer::RemoveFromRot(DWORD reg)
{
    IRunningObjectTable *pROT;
    if (SUCCEEDED(GetRunningObjectTable(0, &pROT)))
	{
        pROT->Revoke(reg);
        pROT->Release();
    }
}


void CPlayer::SetFile(WCHAR *file)
{
	lstrcpy(m_file, file);
}

LONGLONG CPlayer::GetDuration()
{
	if (!m_ms)
		return 0;

	LONGLONG duration;
	HRESULT hr = m_ms->GetDuration(&duration);
	if (FAILED(hr))
		return 0;

	return duration;
}

LONGLONG CPlayer::GetPosition()
{
	if (!m_ms)
		return 0;

	LONGLONG position, stop;
	HRESULT hr = m_ms->GetPositions(&position, &stop);
	if (FAILED(hr))
		return 0;

	return position;
}

void CPlayer::SetPosition(LONGLONG position)
{
	if (!m_ms)
		return;

	HRESULT hr = m_ms->SetPositions(&position, AM_SEEKING_AbsolutePositioning, NULL, AM_SEEKING_NoPositioning);
	if (FAILED(hr))
		return ;

}

int CPlayer::GetAudioTracksCount()
{
	if (!m_ivlc)
		return -1;

	int count = 0;
	HRESULT hr = m_ivlc->GetAudioTracksCount(&count);
	if (FAILED(hr))
		return -1;

	return count;
}

int CPlayer::GetSubtitlesCount()
{
	if (!m_ivlc)
		return -1;

	int count = 0;
	HRESULT hr = m_ivlc->GetSubtitlesCount(&count);
	if (FAILED(hr))
		return -1;

	return count;
}

void CPlayer::GetAudioTrackInfo(int idx, int *id, WCHAR *text)
{
	if (m_ivlc)
		m_ivlc->GetAudioTrackInfo(idx, id, text);
}

void CPlayer::GetSubtitleInfo(int idx, int *id, WCHAR *text)
{
	if (m_ivlc)
		m_ivlc->GetSubtitleInfo(idx, id, text);
}

int CPlayer::GetAudioTrack()
{
	if (!m_ivlc)
		return -1;

	int id = 0;
	HRESULT hr = m_ivlc->GetAudioTrack(&id);
	if (hr != S_OK)
		return -1;

	return id;
}

int CPlayer::GetSubtitle()
{
	if (!m_ivlc)
		return -1;

	int id = 0;
	HRESULT hr = m_ivlc->GetSubtitle(&id);
	if (hr != S_OK)
		return -1;

	return id;
}

int CPlayer::SetAudioTrack(int id)
{
	if (!m_ivlc)
		return -1;

	HRESULT hr = m_ivlc->SetAudioTrack(id);
	if (hr != S_OK)
		return -1;

	return 0;
}

int CPlayer::SetSubtitle(int id)
{
	if (!m_ivlc)
		return -1;

	HRESULT hr = m_ivlc->SetSubtitle(id);
	if (hr != S_OK)
		return -1;

	return 0;
}